import java.io.IOException;
import java.util.Scanner;

public class Main {
    static final String CSV_FILE_NAME = "src/main/resources/employees.csv";
    static final String JSON_FILE_NAME = "src/main/resources/employees.json";

    public static void main(String[] args) throws IOException {
        Scanner in = new Scanner(System.in);
        System.out.println("Select type of file (csv or json):");
        String fileName = in.nextLine();
        DataScanner dataScanner = getDataScanner(fileName);
        EmployeeService employeeService = new EmployeeService(dataScanner.readData());
        System.out.println("Select job to sum salary of all employees on this position (Teacher, Janitor, Priest):");
        String job = in.nextLine();
        System.out.println(employeeService.calculateSalaryByJob(job));
    }

    private static DataScanner getDataScanner(String fileName) {
        if (fileName.equals("csv")) {
            return new CSVScanner(CSV_FILE_NAME);
        } else if (fileName.equals("json")) {
            return new JsonScanner(JSON_FILE_NAME);
        } else {
            throw new IllegalArgumentException("No file found with the name: " + fileName);
        }
    }
}

