import java.math.BigDecimal;
import java.util.List;

public class EmployeeService {

    private final List<Employee> employeeList;

    public EmployeeService(List<Employee> employeeList) {
        this.employeeList = employeeList;
    }

    public BigDecimal calculateSalaryByJob(String jobName) {
        if (checkIfJobExist(jobName)) {
            return employeeList.stream()
                    .filter(e -> e.getJob().equals(jobName))
                    .map(Employee::getSalary)
                    .map(x -> x.replaceAll(",", "."))
                    .map(BigDecimal::new)
                    .reduce(BigDecimal.ZERO, BigDecimal::add);
        } else {
            throw new IllegalArgumentException("No job found with the name: " + jobName);
        }
    }

    private Boolean checkIfJobExist(String jobName) {
        return employeeList.stream()
                .anyMatch(e -> e.getJob().equals(jobName));
    }
}
