import com.opencsv.bean.CsvToBeanBuilder;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

public class CSVScanner implements DataScanner {

    private final String fileName;

    public CSVScanner(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public List<Employee> readData() throws IOException {
        return new CsvToBeanBuilder(new FileReader(fileName))
                .withType(Employee.class)
                .withSeparator(';')
                .withIgnoreLeadingWhiteSpace(true)
                .build()
                .parse();
    }

}
