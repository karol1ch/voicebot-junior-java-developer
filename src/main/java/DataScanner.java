import java.io.IOException;
import java.util.List;

public interface DataScanner {
    List<Employee> readData() throws IOException;
}
