import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class JsonScanner implements DataScanner {

    private final String fileName;

    public JsonScanner(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public List<Employee> readData() throws IOException {
        return new ObjectMapper()
                .readValue(Paths.get(fileName).toFile(), new TypeReference<Map<String, List<Employee>>>() {})
                .values()
                .stream()
                .flatMap(List::stream)
                .collect(Collectors.toList());
    }
}
