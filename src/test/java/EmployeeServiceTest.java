import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeServiceTest {

    @Test
    void shouldThrowExceptionWhenJobNotFound() {
        //GIVEN
        Employee employee1 = Employee.employeeBuilder(1, "Bob", "Smith", "Priest", "2500.20");
        Employee employee2 = Employee.employeeBuilder(2, "Jack", "Teddy", "Teacher", "2000,20");
        List<Employee> employeeList = List.of(employee1, employee2);
        EmployeeService employeeService = new EmployeeService(employeeList);

        //WHEN + THEN
        assertThrows(IllegalArgumentException.class, () -> employeeService.calculateSalaryByJob("Miss Marvel"));
    }

    @Test
    void shouldCalculateSumOfSalaries() {
        //GIVEN
        Employee employee1 = Employee.employeeBuilder(1, "Bob", "Smith", "Priest", "2500.20");
        Employee employee2 = Employee.employeeBuilder(2, "Jack", "Teddy", "Teacher", "2000,20");
        Employee employee3 = Employee.employeeBuilder(3, "Mike", "Ross", "Teacher", "3000,80");
        List<Employee> employeeList = List.of(employee1, employee2, employee3);
        EmployeeService employeeService = new EmployeeService(employeeList);

        //WHEN
        BigDecimal sum = employeeService.calculateSalaryByJob("Teacher");

        //THEN
        assertEquals(new BigDecimal("5001.00"), sum);
    }
}